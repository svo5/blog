import { Body, Controller, Post } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { BaseController } from '../common/base';
import { AuthService } from './auth.service';
import { CreateResponseDto, CreateUserDTO} from '../users/DTO';

@ApiBearerAuth()
@Controller('auth')
@ApiTags('Auth')
export class AuthController extends BaseController {
    constructor(private readonly authService: AuthService) {
        super();
    }

    @Post('/login')
    login(@Body() user: CreateUserDTO ){
        return this.authService.login(user)
    }

    @Post('/registration')
    registration(@Body() userDto: CreateUserDTO ): Promise<CreateResponseDto>{
        return this.authService.registration(userDto)
    }
}

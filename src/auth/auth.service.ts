import {ForbiddenException, Injectable} from '@nestjs/common';
import {JwtService} from '@nestjs/jwt';
import * as bcrypt from 'bcryptjs';

import {CreateResponseDto, CreateUserDTO} from '../users/DTO';
import {UsersService} from '../users/users/users.service';
import {RegistrationResponseDto} from './DTO';


@Injectable()
export class AuthService {
  constructor(private usersService: UsersService,
    private jwtService: JwtService) {
  }

  async login( userDto: CreateUserDTO ): Promise<RegistrationResponseDto>{
    const user = await this.validateUser(userDto);
    return this.generateToken(user)
  }

  async registration( userDto: CreateUserDTO ):Promise<CreateResponseDto>{
    const candidate = await this.usersService.getUsersByEmail(userDto.email);
    if (candidate) {
      throw new ForbiddenException(`User with email ${candidate.email} already exist`);
    }
    const hashPassword = await bcrypt.hash(userDto.password, 10);
    return await this.usersService.create({...userDto, password: hashPassword})
  }

  private async generateToken(user): Promise<RegistrationResponseDto>{
    const payload = { email: user.email, id: user.id }
    return {
      token: this.jwtService.sign(payload)
    }
  }

  private async validateUser(userDto: CreateUserDTO) {
    const user = await this.usersService.getUsersByEmail(userDto.email);
    const passwordEquals = await bcrypt.compare(userDto.password, user.password);
    if (user && passwordEquals){
      return user
    } else {
      throw new ForbiddenException(`Wrong email or password`);    }
  }
}

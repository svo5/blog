import {forwardRef, Module} from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Users } from '../users/users/users.entity';
import { Posts } from './post.enyity';
import { PostService } from './post.service';
import { PostController } from './post.controller';
import { FilesModule } from '../files/files.module';
import { UsersModule } from '../users/users/users.module';
import { AuthModule } from '../auth/auth.module';

@Module({
  providers: [ PostService, ],
  controllers: [ PostController, ],
  imports: [
      TypeOrmModule.forFeature([ Users, Posts, ]),
      FilesModule,
      UsersModule,
      forwardRef(() =>AuthModule)
  ]
})
export class PostModule {}

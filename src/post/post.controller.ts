import { Body, Controller, Delete, Post, Put, Query, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiBearerAuth, ApiConsumes, ApiCreatedResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

import { CreatePostDto, DeletePostDto, ResponsePostDto } from './DTO';
import { AuthUser } from '../auth/decorators';
import { Person } from '../auth/models';
import { PostService } from './post.service';
import { Posts } from './post.enyity';
import { BaseController, BaseMessageDTO } from '../common/base';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiFile } from '../common/decorators';


@ApiBearerAuth()
@Controller('posts')
@ApiTags('Posts')
export class PostController extends BaseController {
  constructor(private readonly postsService: PostService) {
    super();
  }

  @ApiOperation({ summary: 'Create post', })
  @UseGuards(JwtAuthGuard)
  @ApiCreatedResponse({ description: 'Returns created post', type: ResponsePostDto, })
  @Post('post')
  createPost(@AuthUser() user: Person, @Body() postDto: CreatePostDto): Promise<ResponsePostDto> {
    return this.postsService.createPost(user.id, postDto, );
  }

  @ApiOperation({ summary: 'Load file', })
  @UseGuards(JwtAuthGuard)
  @ApiFile('file')
  @ApiConsumes('multipart/form-data')
  @ApiCreatedResponse({ description: 'Returns created post', type: ResponsePostDto, })
  @Post('load-file')
  @UseInterceptors(FileInterceptor('file', ))
  loadFile(@AuthUser() user: Person, @UploadedFile() file): Promise<ResponsePostDto> {
    return this.postsService.loadFile(user.id, file);
  }

  @ApiOperation({ summary: 'Update post', })
  @UseGuards(JwtAuthGuard)
  @ApiCreatedResponse({ description: 'Returns update post', type: BaseMessageDTO, })
  @Put('update-post')
  updatePost(@AuthUser() user: Person, @Query() postDto: DeletePostDto, @Body() contentDto: CreatePostDto): Promise<ResponsePostDto> {
    return this.postsService.updatePost(user.id, postDto.postId, contentDto.content);
  }

  @ApiOperation({ summary: 'Load file', })
  @UseGuards(JwtAuthGuard)
  @ApiFile('file')
  @ApiConsumes('multipart/form-data')
  @ApiCreatedResponse({ description: 'Returns created post', type: ResponsePostDto, })
  @Put('update-file')
  @UseInterceptors(FileInterceptor('file', ))
  updateFile(@AuthUser() user: Person, @Query() postDto: DeletePostDto, @UploadedFile() file): Promise<ResponsePostDto> {
    return this.postsService.updateFile(user.id, postDto.postId, file);
  }

  @ApiOperation({ summary: 'Update post', })
  @UseGuards(JwtAuthGuard)
  @ApiCreatedResponse({ description: 'Returns update post', type: ResponsePostDto, })
  @Delete('post')
  deletePost(@AuthUser() user: Person, @Query() postDto: DeletePostDto): Promise<BaseMessageDTO> {
    return this.postsService.deletePost(user.id, postDto);
  }
}

import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';

import { BaseEntity } from '../common/base';
import { Users } from "../users/users/users.entity";

@Entity('posts')
export class Posts extends BaseEntity {
    @Column({ default: null })
    content: string;

    @Column({ default: null })
    fileName: string;

    @ManyToOne(() => Users)
    @JoinColumn({ name: 'user_id', })
    users: Users;
}

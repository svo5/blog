import { ForbiddenException, forwardRef, Inject, Injectable } from '@nestjs/common';
import { CreatePostDto, DeletePostDto, ResponsePostDto } from './DTO';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository} from 'typeorm';
import { Posts } from './post.enyity';
import { UsersService } from '../users/users/users.service';
import { FilesService } from '../files/files.service';
import { BaseMessageDTO } from "../common/base";

@Injectable()
export class PostService {
    constructor(
        @InjectRepository(Posts)
        private postsRepository: Repository<Posts>,
        @Inject(forwardRef(() => UsersService))
        private readonly usersService: UsersService,
        @Inject(forwardRef(() => FilesService))
        private readonly filesService: FilesService
    ) {}

  async createPost(userId: number, postDto: CreatePostDto): Promise<ResponsePostDto> {
    const users = await this.usersService.getUserById(userId);
    const post = this.postsRepository.create({ ...postDto, users });
      delete post.users;
    await this.postsRepository.save(post);
      return { ...post, userId: users.id }
    }

  async loadFile(userId: number, file: any): Promise<ResponsePostDto> {
    const fileName = await this.filesService.createFile(file);
    const users = await this.usersService.getUserById(userId);
    const post = this.postsRepository.create({ users, fileName, });
    await this.postsRepository.save(post);
      return { ...post, userId }
    }

  async deletePost(id: number, postDto: DeletePostDto): Promise<BaseMessageDTO> {
    const post = await this.postsRepository.findOne({ where: { id: postDto.postId, deletedAt: null }, relations: [ 'users', ],})
    if (!post || post.users.id != id ){
      throw new ForbiddenException(`Post is not exist or this post does not belong to the user`);
    }
      await this.postsRepository.softDelete(post.id);
      return  { message: 'The entity was successfully deleted', };
    }

    async updatePost(userId: number, postId: number, content: string): Promise<ResponsePostDto> {
      const post = await this.postsRepository.findOne({ where: { id: postId, deletedAt: null }, relations: [ 'users', ], });
        if (!post || post.users.id != userId ){
          throw new ForbiddenException(`Post is not exist or this post does not belong to the user`);
        }
        const createUpdatePost = this.postsRepository.create({ ...post, content, });
        delete createUpdatePost.users;
        await this.postsRepository.save(createUpdatePost);
        return { ...createUpdatePost, userId }
    }

    async updateFile(userId: number, postId: number, file) {
      const post = await this.postsRepository.findOne({ where: { id: postId, deletedAt: null }, relations: [ 'users', ], });
      if (!post || post.users.id != userId ){
        throw new ForbiddenException(`Post is not exist or this post does not belong to the user`);
      }
      const fileName = await this.filesService.createFile(file);
      const createUpdatePost = this.postsRepository.create({ ...post, fileName, });
      delete createUpdatePost.users;
      await this.postsRepository.save(createUpdatePost);
        return { ...createUpdatePost, userId }
    }
}

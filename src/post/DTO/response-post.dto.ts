import { ApiProperty } from '@nestjs/swagger';
import { BaseResponseDTO } from '../../common/base/base-dto';

export class ResponsePostDto extends BaseResponseDTO{
    @ApiProperty()
    content: string;

    @ApiProperty()
    fileName: string;

    @ApiProperty()
    userId: number;
}

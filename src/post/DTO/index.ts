import { CreatePostDto } from './create-post.dto';
import { ResponsePostDto } from './response-post.dto';
import { DeletePostDto } from './delete.post.dto';

export {
    CreatePostDto,
    ResponsePostDto,
    DeletePostDto
}

import { ApiProperty } from '@nestjs/swagger';
import {
    IsInt,
    IsNotEmpty, IsNumber,
} from 'class-validator';

export class DeletePostDto {
    @IsNumber()
    @IsInt()
    @IsNotEmpty()
    @ApiProperty()
    postId: number;
}

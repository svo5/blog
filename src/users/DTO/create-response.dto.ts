import { ApiProperty } from '@nestjs/swagger';

import { BaseResponseDTO } from '../../common/base/base-dto';

export class CreateResponseDto extends BaseResponseDTO {
  @ApiProperty()
  nickname: string;

  @ApiProperty()
  email: string;
}

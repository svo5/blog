import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class SetPasswordDTO {
  @IsNumber()
  @IsInt()
  @IsNotEmpty()
  @ApiProperty({ example:'4', description: 'User identifier' })
  userId: number

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example:'315dfki', description: 'User password' })
  password: string;
}

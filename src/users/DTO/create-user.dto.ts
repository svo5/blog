import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsString
} from 'class-validator';

export class CreateUserDTO {
  @IsString()
  @IsOptional()
  @ApiProperty({ example:'Bartlby', description: 'User nickname' })
  nickname: string;

  @IsEmail()
  @IsNotEmpty()
  @ApiProperty({ example:'Bartlby@gmail.com', description: 'User email' })
  email: string;

  @IsEmail()
  @IsNotEmpty()
  @ApiProperty({ example:'315dfki', description: 'User password' })
  password: string;
}

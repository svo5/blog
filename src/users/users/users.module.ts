import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from "@nestjs/typeorm";

import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { Users } from './users.entity';
import { AuthModule } from '../../auth/auth.module';
import { Posts } from '../../post/post.enyity';

@Module({
  controllers: [ UsersController, ],
  providers: [ UsersService, Users, ],
  imports: [
    TypeOrmModule.forFeature([ Users, Posts, ]),
    forwardRef(() =>AuthModule )
  ],
  exports: [ UsersService, ]
})

export class UsersModule {}

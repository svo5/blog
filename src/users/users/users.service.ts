import { ForbiddenException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { BaseMessageDTO } from '../../common/base';
import { CreateResponseDto, CreateUserDTO, SetPasswordDTO } from '../DTO';
import { Users } from './users.entity';

@Injectable()
export class UsersService {
  constructor(
      @InjectRepository(Users)
      private userRepository: Repository<Users>,
  ) {}

  async create(body: CreateUserDTO): Promise<CreateResponseDto> {
    const createUser = await this.userRepository.create(body);
    return await this.userRepository.save(createUser);
  }

  async getUserById(userId): Promise<Users> {
     const user = await this.userRepository.findOne({ where: { id: userId, }, });
    if (!user) {
      throw new ForbiddenException(`User with id = ${userId} is not exist`);
    }
    return user
  }

  async setPassword(query: SetPasswordDTO): Promise<BaseMessageDTO> {
    await this.userRepository.update(query.userId, {
      password: query.password,
    });
      return { message: 'Password is set successfully', };
  }

  async getUsersByEmail(email: string){
      return await this.userRepository.findOne({ where: { email, } })
  }
}

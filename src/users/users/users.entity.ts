import {Column, Entity, OneToMany,} from 'typeorm';

import { BaseEntity } from '../../common/base';
import { Posts } from '../../post/post.enyity';

@Entity('users')
export class Users extends BaseEntity {
  @Column()
  nickname: string;

  @Column()
  email: string;

  @Column()
  password: string;

  @OneToMany(() => Posts, posts => posts.users)
  posts: Array<Posts>;
}

import { ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Body, Controller, Patch, Post, Query, UseGuards } from '@nestjs/common';

import { BaseController, BaseMessageDTO } from '../../common/base';
import { UsersService } from './users.service';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import {
    CreateResponseDto,
  CreateUserDTO,
    SetPasswordDTO,
} from '../DTO';


@Controller('users')
@ApiTags('Users')
export class UsersController extends BaseController {
  constructor(private readonly usersService: UsersService) {
    super();
  }
  @ApiOperation({ summary: 'Create user', })
  @ApiCreatedResponse({ description: 'Returns created user', type: CreateResponseDto, })
  @Post('register')
  create(@Body() registerRequestDto: CreateUserDTO): Promise<CreateResponseDto> {
    return this.usersService.create(registerRequestDto);
  }

  @ApiOperation({ summary: 'Set password', })
  @UseGuards(JwtAuthGuard)
  @ApiOkResponse({ description: 'Returns status of setting the password', type: BaseMessageDTO, })
  @Patch('set-password')
  setPassword(@Query() query: SetPasswordDTO): Promise<BaseMessageDTO> {
    return this.usersService.setPassword(query);
  }
}

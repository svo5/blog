import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class AuthenticationCodeDTO {
    @IsNotEmpty()
    @ApiProperty()
    authenticationCode: string;
}

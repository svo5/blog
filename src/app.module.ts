import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
require('dotenv').config({ path:'../blog/.env' });
import { ServeStaticModule } from '@nestjs/serve-static';
import * as path from 'path';

import { UsersModule } from './users/users/users.module';
import { Users } from './users/users/users.entity';
import { AuthModule } from './auth/auth.module';
import { PostModule } from './post/post.module';
import { Posts } from './post/post.enyity';
import { FilesModule } from './files/files.module';



@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: `.${process.env.NODE_ENV}.env`
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.POSTGRES_HOST,
      port: Number(process.env.POSTGRES_PORT),
      username: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB,
      entities: [ Users, Posts, ],
      synchronize: true,
      autoLoadEntities: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: path.resolve(__dirname, 'static'),
    }),
      UsersModule,
      AuthModule,
      PostModule,
      FilesModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
